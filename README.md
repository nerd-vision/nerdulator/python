# Nerdulator - Python

This is the python implementation of the nerdulator nerd.vision calculator demo API.

## Build

To build use the Dockerfile

```bash
docker build -t nerdulator/python .
```

## Run

### Docker
To run the image from the command line you can use this command
```bash
docker run -d -p 9998:9998 -e NV_API_KEY=yourapikey nerdvision/nerdulator:python
```
or using gitlab registry
```bash
docker run -d -p 9998:9998 -e NV_API_KEY=yourapikey registry.gitlab.com/nerd-vision/nerdulator/python
```

### Docker compose
To run the API use docker-compose

```bash
docker-compose up
```

## Configure

To configure the agent, change the NV_API_KEY key in the docker-compose.

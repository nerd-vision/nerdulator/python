FROM python:3.6

WORKDIR /usr/src/app

ADD requirements.txt requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

ADD src/nerdulator.py /usr/src/app/src/nerdulator.py

EXPOSE 9998

CMD ["python", "/usr/src/app/src/nerdulator.py"]

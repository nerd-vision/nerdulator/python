import nerdvision
import tornado.ioloop
import tornado.web

nerdvision.start(
    name="PythonDemo",
    agent_settings={})


class BaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Accept, X-Requested-With, Authorization, Content-Type, X-XSRF-TOKEN")
        self.set_header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, OPTIONS, DELETE')

    def log_exception(self, typ, value, tb):
        nerdvision.capture_exception(typ, value, tb)


class PlusHandler(BaseHandler):
    def get(self):
        int1 = int(self.get_argument("int1", default=None))
        int2 = int(self.get_argument("int2", default=None))
        result = int1 + int2
        self.write({
            "int1": int1,
            "int2": int2,
            "operation": "+",
            "result": result,
            "equation": "%s + %s" % (int1, int2)
        })


class SubtractHandler(BaseHandler):
    def get(self):
        int1 = int(self.get_argument("int1", default=None))
        int2 = int(self.get_argument("int2", default=None))
        result = int2 - int1
        self.write({
            "int1": int1,
            "int2": int2,
            "operation": "-",
            "result": result,
            "equation": "%s - %s" % (int1, int2)
        })


class DivideHandler(BaseHandler):
    def get(self):
        int1 = int(self.get_argument("int1", default=None))
        int2 = int(self.get_argument("int2", default=None))
        result = int1 / int2
        self.write({
            "int1": int1,
            "int2": int2,
            "operation": "/",
            "result": result,
            "equation": "%s / %s" % (int1, int2)
        })


class MultiplyHandler(BaseHandler):
    def get(self):
        int1 = int(self.get_argument("int1", default=None))
        int2 = int(self.get_argument("int2", default=None))
        result = int1 * int2
        self.write({
            "int1": int1,
            "int2": int2,
            "operation": "*",
            "result": result,
            "equation": "%s * %s" % (int1, int2)
        })


def make_app():
    return tornado.web.Application([
        (r"/add", PlusHandler),
        (r"/divide", DivideHandler),
        (r"/multiply", MultiplyHandler),
        (r"/subtract", SubtractHandler)
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(9998)
    tornado.ioloop.IOLoop.current().start()
